-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mydb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `CATEGORY`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CATEGORY` ;

CREATE TABLE IF NOT EXISTS `CATEGORY` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SUPPLIER`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SUPPLIER` ;

CREATE TABLE IF NOT EXISTS `SUPPLIER` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `companyname` VARCHAR(255) NOT NULL,
  `contactname` VARCHAR(255) NOT NULL,
  `adress` TINYTEXT NOT NULL,
  `city` VARCHAR(255) NOT NULL,
  `postalcode` VARCHAR(45) NOT NULL,
  `country` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PRODUCT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PRODUCT` ;

CREATE TABLE IF NOT EXISTS `PRODUCT` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `category` INT NOT NULL,
  `supplier` INT NOT NULL,
  `unit` VARCHAR(255) NOT NULL,
  `price` FLOAT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_PRODUCT_CATEGORY`
    FOREIGN KEY (`category`)
    REFERENCES `CATEGORY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PRODUCT_SUPPLIER1`
    FOREIGN KEY (`supplier`)
    REFERENCES `SUPPLIER` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_PRODUCT_CATEGORY_idx` ON `PRODUCT` (`category` ASC) VISIBLE;

CREATE INDEX `fk_PRODUCT_SUPPLIER1_idx` ON `PRODUCT` (`supplier` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
