select * from customers;

update customers set country=replace(country,'\n','');
update customers set city=replace(city,'\n','');

create view mexicanCustomers as
select customerid ,customername,contactname
from customers
where country= "Mexico";

select * from mexicanCustomers;

select * 
from mexicanCustomers join orders on mexicanCustomers.customerid=orders.customerid;

create view productsbelowavg as
select productid, productname,price
from products
where price < (select avg(price) from products);

#delete from orderdetails;  it will delete all of the rows
delete from orderdetails where ProductID=5;
truncate orderdetails; 

delete from customers;
delete from orders;

drop table customers;





