call getCustomersByCountry("Mexico");

set @country = "UK";
select @country;
call getCustomersByCountry(@country);
select @country;

select * from shippers;

select count(orderID)
from orders join shippers on orders.ShipperID = shippers.ShipperID
where ShipperName = "Speedy Express";

call getNumberOfOrdersByShipper("Speedy Express");

set @orderCount = 0;
call getNumberOfOrdersByShipper("Speedy Express", @orderCount);
select @orderCount;

set @beg = 100;
set @inc = 10;

call counter(@beg, @inc);
select @beg;

select * from movies;
select * from denormalized;

load data 
infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\denormalized.csv"
into table denormalized
columns terminated by ';';
select * from denormalized;

show variables like "secure_file_priv";
insert into movies (movie_id, title, ranking, rating, year, votes, duration, oscars, budget)
select distinctrow movie_id, title, ranking, rating, year, votes, duration, oscars, budget
from denormalized;

select distinctrow producer_country_id, producer_country_name
from denormalized
union
select distinctrow director_country_id, director_country_name
from denormalized
order by producer_country_id;

select * from countries;


